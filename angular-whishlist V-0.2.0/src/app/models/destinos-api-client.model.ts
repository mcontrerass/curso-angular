import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';

export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor() {
		this.destinos = [];
	}
	add(dest: DestinoViaje) {
		this.destinos.push(dest);
	}
	getAll(): DestinoViaje[] {
		return this.destinos;
	}
	getById(id: String): DestinoViaje {
		return this.destinos.filter(function (dest) { return dest.id.toString() === id; })[0];
	}
	elegir(dest: DestinoViaje) {
		this.destinos.forEach(x => x.setSelected(false));
		dest.setSelected(true);
		this.current.next(dest);
	}
	subscribeOnChange(fn) {
		this.current.subscribe(fn);
	}
}