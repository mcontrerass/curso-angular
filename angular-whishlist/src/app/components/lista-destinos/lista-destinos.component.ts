import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { state } from '@angular/animations';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  // Con Redux
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }
  //

  /* Con API y Sin Redux
  constructor(private destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((dest: DestinoViaje) => {
      if (dest != null) {
        this.updates.push('Se ha elegido a ' + dest.nombre);
      }
    });
  }
  */

  ngOnInit() {
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const f = data;
        if (f != null) {
          this.updates.push('Se ha elegido a ' + f.nombre);
        }    
    });
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  agregado(dest: DestinoViaje) {
    this.destinosApiClient.add(dest);
    this.onItemAdded.emit(dest);
  }

  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }

  getAll() {

  }
}
